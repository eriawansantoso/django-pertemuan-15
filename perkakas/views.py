from django.shortcuts import render
from .models import Peralatan  
from django.views.generic import UpdateView, DeleteView, View
from django.views.generic import DetailView, CreateView
from django.urls import reverse_lazy
from .utils import Render




# Create your views here.
var = {
    'judul': 'Toko Pertukangan Aneka Kerja',
    'info': '''Kami menyediakan segala peralatan dan perkakas pertukangan Anda,
        baik untuk hobi maupun untuk profesi anda''',
    'oleh': 'owner'
}
def index(self):
    # new
    var['peralatan'] = Peralatan.objects.values('id', 'nama_alat', 'kategori').\
        order_by('nama_alat')
    return render(self, 'perkakas/index.html', context=var)

class AlatDetailView(DetailView):
    model = Peralatan
    template_name = 'perkakas/alat_detail_view.html'

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context

class AlatCreateView(CreateView):
    model = Peralatan
    fields = '__all__'
    template_name = 'perkakas/alat_add.html'

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context

class AlatEditView(UpdateView):
    model = Peralatan
    fields = ['nama_alat', 'kategori', 'keterangan',
              'berat', 'harga', 'jumlah']
    template_name = 'perkakas/alat_edit.html'

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context

class AlatDeleteView(DeleteView):
    model = Peralatan
    template_name = 'perkakas/alat_delete.html'
    success_url = reverse_lazy('home_page')

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context

class AlatToPdf(View):
    def get(self, request):
        var = {
            'peralatan': Peralatan.objects.values(
                'nama_alat', 'kategori', 'harga', 'keterangan'),
            'request': request
        }
        return Render.to_pdf(self, 'perkakas/alat_to_pdf.html', var)


